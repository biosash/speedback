# Generated by sila2.code_generator; sila2.__version__: 0.10.1
from __future__ import annotations

from typing import NamedTuple


class GetTemperature_Responses(NamedTuple):

    Temperature: float
    """
    Readout of current temperature value
    """


class GetPressure_Responses(NamedTuple):

    Pressure: float
    """
    Readout of current temperature value
    """
